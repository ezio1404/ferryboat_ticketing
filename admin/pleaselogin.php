<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>PleaseLogin</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form class="form-signin" method="POST" action="../controller/admin/admin.log.php">
      <img class="mb-4" src="../images/boatblocked.svg" alt="" width="100" height="100">
      <h1 class="h3 mb-3 font-weight-normal">Please Login</h1>

     <a href="index.php"><em>Go back</em></a>
      <p class="mt-5 mb-3 text-muted">&copy; Ferry Boat 2017-2018</p>
    </form>
  </body>
</html>
