
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form class="form-signin" method="POST" action="../controller/admin/admin.log.php">
      <img class="mb-4" src="../images/boat.svg" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Admin Login</h1>
      <label for="email" class="sr-only">Username</label>
      <input type="text" id="email" name="email" class="form-control" placeholder="Username" required autofocus>
      <label for="password" class="sr-only">Password</label>
      <input type="password" id="password" name="password"  class="form-control" placeholder="Password" required>
        
      <input class="btn btn-lg btn-primary btn-block" type="submit" name="loginAdmin">
      <p class="mt-5 mb-3 text-muted">&copy; Ferry Boat 2017-2018</p>
    </form>
  </body>
</html>
