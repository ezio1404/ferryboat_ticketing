<?php
include '../../model/shipModel.php';
if ($_SESSION['info'] == "admin") {
    $ships = new Ship();
    $ship = $ships->getShipById($_GET['id']);
    ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FerryBoat Admin</title>
    <?php include 'component/style.php'; ?>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'component/nav.php' ?>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Ships</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Striped Rows
                            </div>

                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <form action="../../controller/admin/admin.ship.php?id=<?php echo $_GET['id'];?>" method="post">
                                        <div class="form-group ">
                                            <label for="ship_name">Ship Name:</label>
                                            <input class="form-control" type="text" name="ship_name" id="ship_name" value="<?php echo $ship['ship_name']?>"
                                                placeholder="Ship Name">
                                        </div>

                                        <div class="form-group ">
                                            <label for="ship_maxseats">Ship Max seats:</label>
                                            <input class="form-control" type="number" name="ship_maxseats" id="ship_maxseats" value="<?php echo $ship['ship_maxseats']?>"
                                                value="150">
                                        </div>

                                        <div class="form-group ">
                                            <label for="ship_status">Ship Status:</label>
                                            <select id="ptype" class="form-control" name="ship_status" required >
                                                <?php
                                                if($ship['ship_status']=="Active"){
                                                ?>
                                                <option selected value="Active">Active</option>
                                                <option value="Inactive">Inactive</option>
                                                <?php
}else{
                                                ?>
 <option  value="Active">Active</option>
                                                <option selected value="Inactive">Inactive</option>
                                                <?php
}
                                                ?>
                                            </select>
                                        </div>
                                        <input type="submit" value="Update Ship" name="updateShip">
                                </form>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>

                </div>

            </div>

        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->











    <?php include 'component/scripts.php'; ?>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->

</body>
<?php

} else {
    header("location:../pleaselogin.php");
}

?>

</html>