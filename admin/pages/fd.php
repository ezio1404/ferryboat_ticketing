<?php
include '../../model/db/dbhelper.php';

if($_SESSION['info']=="admin"){

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FerryBoat Admin</title>
<?php include 'component/style.php';?>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'component/nav.php'?>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Fare And Discounts</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Striped Rows
                        </div>
                        <div class="panel-body">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fareModal">
  Add Fare
</button>
<?php include 'component/addModal.php';?>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Fare fee</th>
                                            <th conlspan="3">Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>14.00</td>
                                            <td>
                                            <button>Update</button>
                                            <button>View</button>
                                            <button>Delete</button>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>12.00</td>
                                            <td>
                                            <button>Update</button>
                                            <button>View</button>
                                            <button>Delete</button>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>8.00</td>
                                            <td>
                                            <button>Update</button>
                                            <button>View</button>
                                            <button>Delete</button>
                                        </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Bordered Table
                        </div>
                        <div class="panel-body">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#discountModal">
  Add Discount
</button>
<?php include 'component/addModal.php';?>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <div class="table-responsive">
                                <table class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Discount Code</th>
                                            <th>%</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>0%</td>
                                            <td>REG</td>
                                            <td>You</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>20%</td>
                                            <td>SNR</td>
                                            <td>@You</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>20%</td>
                                            <td>PWD</td>
                                            <td>You</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>


                <!-- /.row -->
               
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include 'component/scripts.php';?>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->

</body>
<?php
}else{
    header("location:../pleaselogin.php");
}

?>
</html>
