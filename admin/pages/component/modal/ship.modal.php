<!--Ships Modal -->
<div class="modal fade" id="classModal" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Ship</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="../../controller/admin/admin.ship.php" method="post">
         <div class="row">
                <div class="form-group ">
                  <label for="ship_name">Ship Name:</label>
                  <input  class="form-control" type="text" name="ship_name" id="ship_name" placeholder="Ship Name">
                </div>
                
                <div class="form-group ">
                  <label for="ship_maxseats">Ship Max seats:</label>
                  <input  class="form-control" type="number" name="ship_maxseats" id="ship_maxseats" value="150">
                </div>
                
                <div class="form-group ">
                  <label for="ship_status">Ship Status:</label>
                  <select id="ptype" class="form-control"  name="ship_status"  required>
                                   
                  <option selected value="Active">Active</option>
                                    <option value="Inactive">Inactive</option>
                                  </select>
                </div>
               
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button">Save changes</button> -->
        <input  class="btn btn-primary" type="submit" value="Add Ship" name="addShip">
      </div>
        </form>
    </div>
  </div>
</div>
<!-- ./ships Modal -->
