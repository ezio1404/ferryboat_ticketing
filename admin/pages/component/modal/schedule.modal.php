<!--sched Modal -->
<div class="modal fade" id="schedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="../../controller/admin/admin.sched.php" method="post">
        <input class="form-control"type="text" name="sched_code" id="sched_code" placeholder="sched_code" required>
        <input class="form-control"type="text" name="sched_origin" id="sched_origin" placeholder="sched_origin" required>
        <input class="form-control" type="text" name="sched_destination" id="sched_destination" placeholder="sched_destination" required>
        <input class="form-control" type="text" name="sched_departure" id="sched_departure" placeholder="sched_departure" required>
        <select class="form-control" name="ship_id" id="ship_id">
          <?php
          $ships = $schedules->getAllShip();
             foreach($ships as $ship){
              echo "<option value=".$ship['ship_id'].">".$ship['ship_name']."</option>";
            }
          ?>
        </select>
        <select class="form-control" name="sched_status" id="sched_status">
          <option value="Boarding">Boarding</option>
          <option value="FullyBooked">Fully Booked</option>
        </select>
        <!-- <input type="submit" value="Add sched" name="addSched"> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        <input type="submit"   class="btn btn-primary" value="Add sched" name="addSched">

      </div>
        </form>
    </div>
  </div>
</div>
<!-- ./sched Modal -->
