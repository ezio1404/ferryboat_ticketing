<?php
include '../../model/shipModel.php';
if ($_SESSION['info'] == "admin") {
    $ships = new Ship();
    $ship = $ships->getAllship();
    ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FerryBoat Admin</title>
    <?php include 'component/style.php'; ?>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'component/nav.php' ?>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Ships</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Striped Rows
                            </div>
                            <div class="panel-body">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#classModal">
                                    add Ship
                                </button>
                                <?php include 'component/modal/ship.modal.php'; ?>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                
                                    <table class="table table-striped table-hover" id="table_id">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Ship Name</th>
                                                <th>Max Seats</th>
                                                <th>Status</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                        foreach ($ship as $s) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo $s['ship_id']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $s['ship_name']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $s['ship_maxseats']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $s['ship_status']; ?>
                                                </td>
                                                <td>
                                                    <a href="update.ship.php?id=<?php echo $s['ship_id']; ?>">Update</a>
                                                    <a href="delete.ship.php?id=<?php echo $s['ship_id']; ?>">Delete</a>
                                                    
                                                </td>
                                            </tr>
                                            <?php

                                        }
                                        ?>
                                        </tbody>
                                    </table>
                             
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>

                </div>

                </div>

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

 
    <?php include 'component/scripts.php'; ?>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
         $(document).ready( function () {
         $('#table_id').DataTable();
         } );
      </script>
</body>
<?php

} else {
    header("location:../pleaselogin.php");
}

?>

</html>