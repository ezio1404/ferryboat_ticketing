<?php
include '../../model/shipModel.php';
if ($_SESSION['info'] == "admin") {
    $ships = new Ship();
    $ship = $ships->getShipById($_GET['id']);
    ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FerryBoat Admin</title>
    <?php include 'component/style.php'; ?>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'component/nav.php' ?>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Ships</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Striped Rows
                            </div>

                            <!-- /.panel-heading -->
                            <div class="panel-body">
                             
<div class="container">

  <div class="panel panel-red">
    <div class="panel-body ">
    <p> DO you want to remove this Ship ?</p>
    <form action="../../controller/admin/admin.ship.php?id=<?php echo $_GET['id'];?>" method="post">
<input type="submit" value="Delete Ship" name="deleteShip">
</form></div>
  </div>
</div>
                                
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>

                </div>

            </div>

        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->











    <?php include 'component/scripts.php'; ?>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->

</body>
<?php

} else {
    header("location:../pleaselogin.php");
}

?>

</html>