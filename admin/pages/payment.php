<?php
require_once '../../model/paymentModel.php';

if ($_SESSION['info'] == "admin") {
    $payment = new Payment();
    $paymentList = $payment->getAllPayment();
    ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FerryBoat Admin</title>
    <?php include 'component/style.php'; ?>

</head>

<body>

    <div id="wrapper">
        <?php include 'component/nav.php' ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Payment</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                DataTables Advanced Tables
                            </div>

                            <!-- /.panel-heading -->
                            <div class="panel-body">

                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th scope="col">Payment ID</th>
                                            <th scope="col">Reserve ID</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                foreach ($paymentList as $paymenteData) {
                                    ?>
                                        <tr>
                                            <td>
                                                <?php echo $paymenteData['payment_id']; ?>
                                            </td>
                                            <td>
                                                <?php echo $paymenteData['rsrv_id']; ?>
                                            </td>
                                            <td>
                                                <?php echo $paymenteData['payment_status']; ?>
                                            </td>
                                            <td>
                                                <?php
                                                    if($paymenteData['payment_status']=="Unpaid"){
                                                        ?>
                                                        <form action="../../controller/admin/admin.payment.php" method="post">
                                                        <input type="number"  hidden name="payment_id" id="payment_id" value="<?php echo $paymenteData['payment_id']; ?>">
                                                        <input type="number"  hidden name="rsrv_id" id="payment_id" value="<?php echo $paymenteData['rsrv_id']; ?>">
                                                        <input type="submit" class="button" name="paid" value="to be paid">
                                                    </form>
                                                        <?php
                                                    }else{echo "PAID";}?>
                                            </td>
                                        </tr>
                                        <?php

}
?>
                                    </tbody>
                                </table>
                                <!-- /.table-responsive -->
  
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include 'component/scripts.php'; ?>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>

</body>

<?php

} else {
    header("location:../pleaselogin.php");
}

?>

</html>