<?php
include '../../model/schedModel.php';

if($_SESSION['info']=="admin"){
    $schedules = new Schedule();
    $sched = $schedules->getAllSched();
    
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FerryBoat Admin</title>
    <?php include 'component/style.php';?>
</head>

<body>

    <div id="wrapper">
<?php include 'component/nav.php'?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Schedules</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                DataTables Advanced Tables
                            </div>
                            <div class="panel-body">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#schedModal">
  Add Schedule
</button>
<?php include 'component/modal/schedule.modal.php';?>
                        </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                        
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                    <tr >
                                        <th>Code</th>
                                        <th>Origin</th>
                                        <th>Desitination</th>
                                        <th>Departure</th>
                                        <th>Ship Name(id)</th>
                                        <th>Status</th>
                                        <th >Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    foreach($sched as $sc){
                                ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $sc['sched_code']?></td>
                                        <td><?php echo $sc['sched_origin']?></td>
                                        <td><?php echo $sc['sched_destination']?></td>
                                        <td><?php echo $sc['sched_departure']?></td>
                                        <td><?php echo $sc['ship_id']?></td>
                                        <td><?php echo $sc['sched_status']?></td>
   
                                        <td>
                                            <button>Update</button>
                                            <button>View</button>
                                            <button>Delete</button>
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
     
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            <div class="well">
                                <h4>DataTables Usage Information</h4>
                                <p>DataTables is a very flexible, advanced tables plugin for jQuery. In SB Admin, we are using a specialized version of DataTables built for Bootstrap 3. We have also customized the table headings to use Font Awesome icons in place of images. For complete documentation on DataTables, visit their website at <a target="_blank" href="https://datatables.net/">https://datatables.net/</a>.</p>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

                <!-- /.row -->
               
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include 'component/scripts.php';?>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

</body>
<?php
}else{
    header("location:../pleaselogin.php");
}

?>
</html>
