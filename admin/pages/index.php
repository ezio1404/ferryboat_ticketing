<?php
session_start();

if($_SESSION['info']=="admin"){

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FerryBoat Admin</title>
    <?php include 'component/style.php';?>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'component/nav.php'?>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Dashboard</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Charts
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                            <canvas id="myChart" height="125"></canvas>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

                <!-- /.row -->
               
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include 'component/scripts.php';?>


<script>
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "Sales of Tickets",
            backgroundColor: 'rgb(137, 207, 240)',
            borderColor: 'rgb(0, 0, 0)',
            data: [1300, 1500, 1400, 1600, 2100, 2500, 2890],
        }]
    },

    // Configuration options go here
    options: {
        
    }
});

</script>
</body>
<?php
}else{
    header("location:../pleaselogin.php");
}

?>
</html>
