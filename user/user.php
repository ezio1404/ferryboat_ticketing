<?php
require_once '../model/passengerModel.php';

if($_SESSION){
  $passenger=new Passenger();
$user=$passenger->getPassengerById($_SESSION['user_id']);
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Business Casual - Start Bootstrap Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/business-casual.min.css" rel="stylesheet">

  </head>

  <body>

    <h1 class="site-heading text-center text-white d-none d-lg-block">
      <!-- <span class="site-heading-upper text-primary mb-3">A Free Bootstrap 4 Business Theme</span> -->
      <span class="site-heading-lower">Business Casual</span>
    </h1>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
      <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-auto">
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="index.php">Home
              </a>
            </li>
            <li class="nav-item  px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="schedule.php">Schedule</a>
            </li>
            <li class="nav-item active px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="user.php">USER <span class="sr-only">(current)</span></a>
            </li>
            <li>
              <form action="../controller/user/user.log.php" method="post">
                  <input class="btn btn-outline-danger" type="submit" value="Logout" name=logout>
              </form>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <section class="page-section about-heading">
      <div class="container">
        <!-- <img class="img-fluid rounded about-heading-img mb-3 mb-lg-0" src="img/log.jpg" alt=""> -->
        <div class="about-heading-content ">
          <div class="row">
            <div class="col-xl-9 col-lg-10 mx-auto">
              <div class="bg-faded rounded p-5">
                <h2 class="section-heading mb-4">
                  <span class="section-heading-upper">Strong Boat, Strong Ferry</span>
                  <span class="section-heading-lower">User Settings</span>
                </h2>
                <form class="needs-validation"  novalidate action="../controller/user/user.controller.php" method="post">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label for="email">Email</label>
                                 <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $user['psngr_email'];?>" required>
                              </div>
                              <div class="form-group col-md-6">
                                 <label for="Password">Password</label>
                                 <input type="password" class="form-control" id="Password" name="password" placeholder="Password" value="<?php echo $user['psngr_password'];?>" required>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="address">Address</label>
                              <input type="text" class="form-control" id="address" name="address" placeholder="221b Baker St." value="<?php echo $user['psngr_address'];?>" required >
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label for="fname">First name</label>
                                 <input type="text" class="form-control" id="fname"  name="fname" placeholder="Marijoy" value="<?php echo $user['psngr_fname'];?>" required>
                              </div>
                              <div class="form-group col-md-6">
                                 <label for="lname">Last name</label>
                                 <input type="text" class="form-control" id="lname"  name="lname" placeholder="Narciso"value="<?php echo $user['psngr_lname'];?>" required>
                              </div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-4">
                                 <label for="inputEmail4">Gender</label>
                                 <select id="gender" class="form-control"  name="gender"  required>
                                   <?php if($user['psngr_gender']=="Male"){
                                     echo "<option selected value='Male'>Male</option>";
                                     echo "<option value='Female'>Female</option>";
                                   }else{
                                     echo "<option  value='Male'>Male</option>";
                                     echo "<option selected value='Female'>Female</option>";
                                   }
                                     ?>
                                 </select>
                              </div>
                              <div class="form-group col-md-4">
                                 <label for="bday">Birthdate</label>
                                 <input type="date" class="form-control" id="bday"  name="bday" value="<?php echo $user['psngr_birthdate'];?>" required>
                              </div>
                              <div class="form-group col-md-4">
                                 <label for="ptype">Passenger Type</label>
                                 <select id="ptype" class="form-control"  name="ptype"  required>
                                   <?php
                                      if($user['psngr_type']=="REG"){
                                   ?>
                                    <option selected value="REG">Regular</option>
                                    <option value="STUD/PWD/SNR">Student/PWD/SNR</option>
                                    <?php
                                      }if($user['psngr_type']=="STUD/PWD/SNR"){
                                 ?>
                                    <option  value="REG">Regular</option>
                                    <option selected value="STUD/PWD/SNR">STUD/PWD/SNR</option>
                                 <?php
                                      }
                                      ?>

                                  </select>
                              </div>
                           </div>
                           <div>
                              <input class="btn btn-primary" type="submit" value="Update" name="update">
                           </div>
                        </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer text-faded text-center py-5">
      <div class="container">
        <p class="m-0 small">Copyright &copy; FerryBoat 2018</p>
      </div>
    </footer>
  </body>

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';
        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');
          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
   </script>
<?php
}else{
  header("location:../index.html?Please_login");
}
?>
</html>
