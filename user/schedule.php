<?php
require_once '../model/passengerModel.php';
require_once '../model/schedModel.php';


if ($_SESSION) {
  $sched = new Schedule();
  $sched_list = $sched->getProcedureSched();

  ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Business Casual - Start Bootstrap Theme</title>
  <!-- Bootstrap core CSS -->
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="../css/business-casual.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
</head>

<body>
  <h1 class="site-heading text-center text-white d-none d-lg-block">
    <!-- <span class="site-heading-upper text-primary mb-3">A Free Bootstrap 4 Business Theme</span> -->
    <span class="site-heading-lower">Business Casual</span>
  </h1>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
    <div class="container">
      <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Start Bootstrap</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav mx-auto">
          <li class="nav-item px-lg-4">
            <a class="nav-link text-uppercase text-expanded" href="index.php">Home
            </a>
          </li>
          <li class="nav-item active px-lg-4">
            <a class="nav-link text-uppercase text-expanded" href="schedule.php">Schedule</a>
          </li>
          <li class="nav-item  px-lg-4">
            <a class="nav-link text-uppercase text-expanded" href="user.php">USER <span class="sr-only">(current)</span></a>
          </li>
          <li>
            <form action="../controller/user/user.log.php" method="post">
              <input class="btn btn-outline-danger" type="submit" value="Logout" name=logout>
            </form>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <section class="page-section about-heading">
    <div class="container">
      <!-- <img class="img-fluid rounded about-heading-img mb-3 mb-lg-0" src="img/log.jpg" alt=""> -->
      <div class="about-heading-content ">
        <div class="row">
          <div class="col-xl-12 col-lg-10 mx-auto">
            <div class="bg-faded rounded p-5 table-responsive-xl">
              <h2 class="section-heading mb-4">
                <span class="section-heading-upper">Strong Boat, Strong Ferry</span>
                <span class="section-heading-lower">Reserve</span>
              </h2>
              <table class="table table-hover table-sm table-striped table-bordered" id="table_id">
                <thead>
                  <tr>
                    <th>Code</th>
                    <th>Origin</th>
                    <th>Desitination</th>
                    <th>Departure</th>
                    <th>Ship Name(id)</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                                foreach ($sched_list as $sc) {
                                  ?>
                  <tr class="odd gradeX">
                    <td>
                      <?php echo $sc['sched_code'] ?>
                    </td>
                    <td>
                      <?php echo $sc['sched_origin'] ?>
                    </td>
                    <td>
                      <?php echo $sc['sched_destination'] ?>
                    </td>
                    <td>
                      <?php echo $sc['sched_departure'] ?>
                    </td>
                    <td>
                      <?php echo $sc['ship_id'] ?>
                    </td>
                    <td>
                      <?php echo $sc['sched_status'] ?>
                    </td>

                    <td>
                      <button type="button" id="reserve" class="btn btn-sm btn-block btn-outline-primary" onclick="openmodal('<?php echo $sc['sched_code']; ?>');"><em>Reserve</em></button></td>
                    </td>
                  </tr>
                  <?php
                              }
                              ?>
                </tbody>
              </table>
              <td>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <footer class="footer text-faded text-center py-5">
    <div class="container">
      <p class="m-0 small">Copyright &copy; Your Website 2018</p>
    </div>
  </footer>
  <div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Reserve</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="../controller/user/user.reserve.php">
            <div class="form-group">
              <div class="form-row">
                <!-- <div class="col-md-6">
                  <label for="reserve_id">Reserve ID</label>
                  <input class="form-control" readonly name="reserve_id" id="reserve_id" type="number" >
                </div> -->
                
                  <label for="sched_code">Schedule Code</label>
                  <input class="form-control" readonly name="sched_code" id="sched_code" type="text" value="">
                
                
                  <label for="psngr_id">Passengger ID</label>
                  <input class="form-control" readonly name="psngr_id" id="psngr_id" type="number" value="<?php echo $_SESSION['user_id'];?>">
                
                
                  <label for="rsrv_date">Reserve Date</label>
                  <input class="form-control"  name="rsrv_date" id="rsrv_date" type="date">
                
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-primary" name="addReserve" value="Reserve">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" charset="utf8" src="../vendor/jquery/jquery.min.js"></script>
  <script type="text/javascript" charset="utf8" src="../vendor/jquery/jquery.dataTables.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script>
    $(document).ready(function () {
      $('#table_id').DataTable();
    });
  </script>
  <script>
    function openmodal(id) {
      $('#sched_code').val(id);
      console.log(id)
      $('#myModal').modal();
    }
  </script>
</body>
<?php

} else {
  header("location:../index.html?Please_login");
}
?>

</html>