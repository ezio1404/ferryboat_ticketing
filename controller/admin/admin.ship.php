<?php
require_once '../../model/shipModel.php';
$ships = new Ship();
    if(isset($_POST['addShip'])){
    $flag=true;
    $name=htmlentities($_POST['ship_name']);
    $maxseats=htmlentities($_POST['ship_maxseats']);
    $status=htmlentities($_POST['ship_status']);
    $shipArray=array($name,$maxseats,$status);
    for($i=0;$i<count($shipArray);$i++){
        if($shipArray[$i]==""){
            $flag=false;
            break;
        }
    }

    if($flag){
        $ships->addShip($shipArray);
        header('location:../../admin/pages/ships.php?success_adding_ship');
     }
     else{
        $message = "Error";
        echo "<script type='text/javascript'>alert('$message');</script>";  
     }
}
if(isset($_POST['updateShip'])){
    $flag=true;
    $name=htmlentities($_POST['ship_name']);
    $maxseats=htmlentities($_POST['ship_maxseats']);
    $status=htmlentities($_POST['ship_status']);
    $shipArray=array($name,$maxseats,$status);
    for($i=0;$i<count($shipArray);$i++){
        if($shipArray[$i]==""){
            $flag=false;
            break;
        }
    }

    if($flag){
        $ships->updateShip($shipArray,$_GET['id']);
        header('location:../../admin/pages/ships.php?success_updating_ship');
     }
     else{
        $message = "Error";
        echo "<script type='text/javascript'>alert('$message');</script>";  
     }
}
if(isset($_POST['deleteShip'])){
    $ships->deleteShip($_GET['id']);
    header('location:../../admin/pages/ships.php?success_udeleting_ship');

}
