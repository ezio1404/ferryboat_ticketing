<?php
require_once '../../model/schedModel.php';
$sched = new Schedule();
    if(isset($_POST['addSched'])){
    $flag=true;
    $sched_code=htmlentities($_POST['sched_code']);
    $sched_origin=htmlentities($_POST['sched_origin']);
    $sched_destination=htmlentities($_POST['sched_destination']);
    $sched_departure=htmlentities($_POST['sched_departure']);
    $ship_id=htmlentities($_POST['ship_id']);
    $sched_status=htmlentities($_POST['sched_status']);
    $schedArray=array($sched_code, $sched_origin,$sched_destination,$sched_departure,$ship_id,$sched_status);
    for($i=0;$i<count($schedArray);$i++){
        if($schedArray[$i]==""){
            $flag=false;
            break;
        }
    }

    if($flag){
        $sched->addSched($schedArray);
        header('location:../../admin/pages/schedules.php?success_adding_sched');
     }
     else{
        $message = "Error";
        echo "<script type='text/javascript'>alert('$message');</script>";  
     }
}