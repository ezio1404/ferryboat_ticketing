<?php
require_once '../../model/passengerModel.php';
$Passenger = new Passenger();
if(isset($_POST['register'])){ 
    $flag=true;
    $fname = htmlentities($_POST['fname']);
    $lname =  htmlentities($_POST['lname']);
    $gender=  htmlentities($_POST['gender']);
    $address=  htmlentities($_POST['address']);
    $ptype=  htmlentities($_POST['ptype']);
    $bday=  htmlentities($_POST['bday']);
    $email =  htmlentities($_POST['email']);
    $password =  htmlentities($_POST['password']);
    $passengerArray=array($fname, $lname, $gender,$address, $ptype,$bday,$email,$password);
    for($i=0;$i<count($passengerArray);$i++){
        if($passengerArray[$i]==""){
            $flag=false;
            break;
        }
    }
    if($flag){
        $Passenger->addPassenger($passengerArray);
        header('location:../../index.html?success_creating');
     }
     else{
        $message = "Invalid Credentials";
        echo "<script type='text/javascript'>alert('$message');</script>";  
     }
}
if(isset($_POST['update'])){ 
    $flag=true;
    $fname = htmlentities($_POST['fname']);
    $lname =  htmlentities($_POST['lname']);
    $gender=  htmlentities($_POST['gender']);
    $address=  htmlentities($_POST['address']);
    $ptype=  htmlentities($_POST['ptype']);
    $bday=  htmlentities($_POST['bday']);
    $email =  htmlentities($_POST['email']);
    $password =  htmlentities($_POST['password']);
    
    $passengerArray=array($fname, $lname, $gender,$address, $ptype,$bday,$email,$password);
    for($i=0;$i<count($passengerArray);$i++){
        if($passengerArray[$i]==""){
            $flag=false;
            break;
        }
    }
    if($flag){
        $Passenger->updatePassenger($passengerArray,$_SESSION['user_id']);
        header('location:../../user/index.php?success_updating');
     }
     else{
        $message = "Invalid Credentials";
        echo "<script type='text/javascript'>alert('$message');</script>";  
     }
}