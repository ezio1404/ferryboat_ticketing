<?php
require_once '../../model/reserveModel.php';
require_once '../../model/paymentModel.php';
$reserve = new Reserve();
$payment = new Payment();
if(isset($_POST['addReserve'])){
    $date=filter($_POST['rsrv_date']);
    $pass_id=filter($_POST['psngr_id']);
    $sched_code=filter($_POST['sched_code']);
    $flag=true;
    $reserveArray=array($date,$pass_id,$sched_code);
    for($i=0;$i<count($reserveArray);$i++){
        if($reserveArray[$i]==""){
            $flag=false;
            break;
        }
    }
    if($flag){
        $reserve->addReserve($reserveArray);
        header('location:../../user/schedule.php?succes_reserve');
        $reserveList=$reserve->getReserveByPassenger($_SESSION['user_id']);
        foreach($reserveList as $rs){
            $last_data=$rs['rsrv_id'];
        }
        $payment->addPayment(array($last_data));
     }

}

function filter($data){
    return trim(htmlentities($data));
}