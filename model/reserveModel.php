<?php
require_once 'db/dbhelper.php';
Class Reserve extends DBHelper{
    private $table = 'tbl_reserve';
    private $fields = array(
        'rsrv_date',
        'psngr_id',
        'sched_code',
    );
    private $Updatefields = array(
        'rsrv_date',
        'psngr_id',
        'sched_code',
        'rsrv_status'
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addReserve($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllReserve(){
     return DBHelper::getAllRecord($this->table);
 }
 function getReserveById($ref_id){
    return DBHelper::getRecordById($this->table,'rsrv_id',$ref_id);
}
function getReserve($ref_id){
    return DBHelper::getRecord($this->table,'rsrv_id',$ref_id);
}
function getReserveByPassenger($ref_id){
    return DBHelper::getRecord($this->table,'psngr_id',$ref_id);
}
// Update
function updateReserve($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->Updatefields,$data,'rsrv_id',$ref_id); 
 }
 // Delete
 function deleteReserve($ref_id){
          return DBHelper::deleteRecord($this->table,'rsrv_id',$ref_id);
}
// Some Functions
    function getCountReserve(){
        return DBHelper::countRecord('rsrv_id',$this->table);
    }
}
?>