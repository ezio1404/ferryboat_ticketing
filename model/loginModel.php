<?php
require_once 'db/dbhelper.php';
Class Login extends DBHelper{
    function __construct(){
        return DBHelper::__construct();
    }
    function login($data){
        return DBHelper::logginUser($data);
    }
}