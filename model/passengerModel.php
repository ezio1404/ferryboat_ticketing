<?php
require_once 'db/dbhelper.php';
Class Passenger extends DBHelper{
    private $table = 'tbl_passenger';
    private $fields = array(
        'psngr_fname',
        'psngr_lname',
        'psngr_gender',
        'psngr_address',
        'psngr_type',
        'psngr_birthdate',
        'psngr_email',
        'psngr_password',
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addPassenger($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllPassenger(){
     return DBHelper::getAllRecord($this->table);
 }
 function getPassengerById($ref_id){
    return DBHelper::getRecordById($this->table,'psngr_id',$ref_id);
}
function getPassenger($ref_id){
    return DBHelper::getRecord($this->table,'psngr_id',$ref_id);
}
// Update
function updatePassenger($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->fields,$data,'psngr_id',$ref_id); 
 }
 // Delete
 function deletePassenger($ref_id){
          return DBHelper::deleteRecord($this->table,'psngr_id',$ref_id);
}
// Some Functions
    function getCountPassenger(){
        return DBHelper::countRecord('psngr_id',$this->table);
    }
}
?>