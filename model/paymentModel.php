<?php
require_once 'db/dbhelper.php';
Class Payment extends DBHelper{
    private $table = 'tbl_payment';
    private $fields = array(
        'rsrv_id',
    );
    private $Updatefields = array(
        'rsrv_id',
        'payment_status'
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addPayment($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllPayment(){
     return DBHelper::getAllRecord($this->table);
 }
 function getPaymentById($ref_id){
    return DBHelper::getRecordById($this->table,'payment_id',$ref_id);
}
function getPayment($ref_id){
    return DBHelper::getRecord($this->table,'payment_id',$ref_id);
}
function getPaymentByPassenger($ref_id){
    return DBHelper::getRecord($this->table,'psngr_id',$ref_id);
}
// Update
function updatePayment($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->Updatefields,$data,'payment_id',$ref_id); 
 }
 // Delete
 function deletePayment($ref_id){
          return DBHelper::deleteRecord($this->table,'payment_id',$ref_id);
}
// Some Functions
    function getCountPayment(){
        return DBHelper::countRecord('payment_id',$this->table);
    }
}
?>