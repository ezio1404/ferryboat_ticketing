<?php
require_once 'db/dbhelper.php';
Class Ship extends DBHelper{
    private $table = 'tbl_ship';
    private $fields = array(
        'ship_name',
        'ship_maxseats',
        'ship_status'
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addShip($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllShip(){
     return DBHelper::getAllRecord($this->table);
 }
 function getShipById($ref_id){
    return DBHelper::getRecordById($this->table,'ship_id',$ref_id);
}
function getShip($ref_id){
    return DBHelper::getRecord($this->table,'ship_id',$ref_id);
}
// Update
function updateShip($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->fields,$data,'ship_id',$ref_id); 
 }
 // Delete
 function deleteShip($ref_id){
          return DBHelper::deleteRecord($this->table,'ship_id',$ref_id);
}
// Some Functions
    function getCountShip(){
        return DBHelper::countRecord('ship_id',$this->table);
    }
}
?>