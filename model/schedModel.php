<?php
require_once 'db/dbhelper.php';
class Schedule extends DBHelper
{
    private $table = 'tbl_schedule';
    private $fields = array(
        'sched_code',
        'sched_origin',
        'sched_destination',
        'sched_departure',
        'ship_id',
        'sched_status'
    );
//constructor
    function __construct()
    {
        return DBHelper::__construct();
    }
// Create
    function addSched($data)
    {
        return DBHelper::insertRecord($data, $this->fields, $this->table);
    }
// Retreive
    function getAllSched()
    {
        return DBHelper::getAllRecord($this->table);
    }
    function getSchedById($ref_id)
    {
        return DBHelper::getRecordById($this->table, 'sched_code', $ref_id);
    }
    function getSched($ref_id)
    {
        return DBHelper::getRecord($this->table, 'sched_code', $ref_id);
    }
    function getAllShip()
    {
        return DBHelper::getAllRecord('tbl_ship');
    }
// Update
    function updateSched($data, $ref_id)
    {
        return DBHelper::updateRecord($this->table, $this->fields, $data, 'sched_code', $ref_id);
    }
 // Delete
    function deleteSched($ref_id)
    {
        return DBHelper::deleteRecord($this->table, 'sched_code', $ref_id);
    }
// Some Functions
    function getCountSched()
    {
        return DBHelper::countRecord('sched_code', $this->table);
    }
    function getProcedureSched()
    {
        return DBHelper::getProcedure($this->table);
    }
}
?>