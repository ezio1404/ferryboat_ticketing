-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2018 at 07:51 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ferryboat_db`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `display_tbl_schedule` ()  SELECT * FROM tbl_schedule$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_passenger`
--

CREATE TABLE `tbl_passenger` (
  `psngr_id` int(11) NOT NULL,
  `psngr_fname` varchar(255) NOT NULL,
  `psngr_lname` varchar(255) NOT NULL,
  `psngr_gender` varchar(255) NOT NULL,
  `psngr_address` varchar(255) NOT NULL,
  `psngr_type` varchar(255) NOT NULL,
  `psngr_birthdate` date NOT NULL,
  `psngr_email` varchar(255) NOT NULL,
  `psngr_password` varchar(255) NOT NULL,
  `psngr_status` varchar(255) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_passenger`
--

INSERT INTO `tbl_passenger` (`psngr_id`, `psngr_fname`, `psngr_lname`, `psngr_gender`, `psngr_address`, `psngr_type`, `psngr_birthdate`, `psngr_email`, `psngr_password`, `psngr_status`) VALUES
(1, 'testing', 'Potot', 'Female', 'Upper Kalunasan CEbu CIty', 'REG', '1998-04-14', 'zuming1404@gmail.com', '12345', 'Active'),
(6, 'alyn', 'magdadaro', 'Female', 'mandaue', 'STUD/PWD/SNR', '1998-09-14', 'am@gmail.com', 'magdadaro', 'Active'),
(7, 'test@test', 'test@test', 'Female', 'test@testtest@test', 'STUD/PWD/SNR', '2018-10-08', 'test@test', 'test', 'Active');

--
-- Triggers `tbl_passenger`
--
DELIMITER $$
CREATE TRIGGER `passenger_created` AFTER INSERT ON `tbl_passenger` FOR EACH ROW insert into tbl_passengercreated(psngr_id) values(NEW.psngr_id)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_passengercreated`
--

CREATE TABLE `tbl_passengercreated` (
  `log_id` int(11) NOT NULL,
  `psngr_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_passengercreated`
--

INSERT INTO `tbl_passengercreated` (`log_id`, `psngr_id`, `date_created`) VALUES
(1, 5, '2018-09-14 10:19:54'),
(2, 6, '2018-09-14 10:23:44'),
(3, 7, '2018-10-07 17:10:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_passengertype`
--

CREATE TABLE `tbl_passengertype` (
  `psngr_type` varchar(255) NOT NULL,
  `psngr_desc` varchar(255) NOT NULL,
  `psngr_fee` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_passengertype`
--

INSERT INTO `tbl_passengertype` (`psngr_type`, `psngr_desc`, `psngr_fee`) VALUES
('REG', 'Regular', '14.00'),
('STUD/PWD/SNR', 'Studen,Senior,PWD', '12.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `payment_id` int(11) NOT NULL,
  `rsrv_id` int(11) NOT NULL,
  `payment_status` varchar(255) NOT NULL DEFAULT 'Unpaid'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`payment_id`, `rsrv_id`, `payment_status`) VALUES
(5, 6, 'Paid'),
(6, 7, 'Paid');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reserve`
--

CREATE TABLE `tbl_reserve` (
  `rsrv_id` int(11) NOT NULL,
  `rsrv_date` date NOT NULL,
  `psngr_id` int(11) NOT NULL,
  `sched_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_reserve`
--

INSERT INTO `tbl_reserve` (`rsrv_id`, `rsrv_date`, `psngr_id`, `sched_code`) VALUES
(6, '2018-10-08', 6, 'CEB-COR'),
(7, '2018-10-08', 7, 'CEB-COR');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_schedule`
--

CREATE TABLE `tbl_schedule` (
  `sched_code` varchar(255) NOT NULL,
  `sched_origin` varchar(255) NOT NULL,
  `sched_destination` varchar(255) NOT NULL,
  `sched_departure` time NOT NULL,
  `ship_id` int(11) NOT NULL,
  `sched_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_schedule`
--

INSERT INTO `tbl_schedule` (`sched_code`, `sched_origin`, `sched_destination`, `sched_departure`, `ship_id`, `sched_status`) VALUES
('CEB-COR', 'cebu', 'cordoba', '16:14:00', 2, 'boarding'),
('CEB-LAP-001', 'CEBU', 'LAPU-LAPU', '00:59:19', 1, 'Boarding');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ship`
--

CREATE TABLE `tbl_ship` (
  `ship_id` int(11) NOT NULL,
  `ship_name` varchar(255) NOT NULL,
  `ship_maxseats` int(11) NOT NULL DEFAULT '200',
  `ship_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ship`
--

INSERT INTO `tbl_ship` (`ship_id`, `ship_name`, `ship_maxseats`, `ship_status`) VALUES
(1, 'Black Pearl', 200, 'Active'),
(2, 'Narciso Ferry 002', 200, 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_passenger`
--
ALTER TABLE `tbl_passenger`
  ADD PRIMARY KEY (`psngr_id`),
  ADD KEY `psngr_type` (`psngr_type`);

--
-- Indexes for table `tbl_passengercreated`
--
ALTER TABLE `tbl_passengercreated`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `psngr_id` (`psngr_id`);

--
-- Indexes for table `tbl_passengertype`
--
ALTER TABLE `tbl_passengertype`
  ADD PRIMARY KEY (`psngr_type`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `rsrv_id` (`rsrv_id`);

--
-- Indexes for table `tbl_reserve`
--
ALTER TABLE `tbl_reserve`
  ADD PRIMARY KEY (`rsrv_id`),
  ADD KEY `psngr_id` (`psngr_id`),
  ADD KEY `sched_code` (`sched_code`);

--
-- Indexes for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  ADD PRIMARY KEY (`sched_code`),
  ADD KEY `ship_id` (`ship_id`);

--
-- Indexes for table `tbl_ship`
--
ALTER TABLE `tbl_ship`
  ADD PRIMARY KEY (`ship_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_passenger`
--
ALTER TABLE `tbl_passenger`
  MODIFY `psngr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_passengercreated`
--
ALTER TABLE `tbl_passengercreated`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_reserve`
--
ALTER TABLE `tbl_reserve`
  MODIFY `rsrv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_ship`
--
ALTER TABLE `tbl_ship`
  MODIFY `ship_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_passenger`
--
ALTER TABLE `tbl_passenger`
  ADD CONSTRAINT `tbl_passenger_ibfk_1` FOREIGN KEY (`psngr_type`) REFERENCES `tbl_passengertype` (`psngr_type`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD CONSTRAINT `tbl_payment_ibfk_1` FOREIGN KEY (`rsrv_id`) REFERENCES `tbl_reserve` (`rsrv_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_reserve`
--
ALTER TABLE `tbl_reserve`
  ADD CONSTRAINT `tbl_reserve_ibfk_1` FOREIGN KEY (`psngr_id`) REFERENCES `tbl_passenger` (`psngr_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_reserve_ibfk_2` FOREIGN KEY (`sched_code`) REFERENCES `tbl_schedule` (`sched_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  ADD CONSTRAINT `tbl_schedule_ibfk_1` FOREIGN KEY (`ship_id`) REFERENCES `tbl_ship` (`ship_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
